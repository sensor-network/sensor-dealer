#!/bin/bash

CRON_SCHEDULE="${CRON_SCHEDULE:-"* * * * *"}"

cat <<EOF > sensor-dealer.yaml
room: ${ROOM}
broker: ${SENSOR_BROKER:-http://broker:5000/sensor}
db: ${SENSOR_DB_CONTROLLER:-http://controller}
EOF

cat <<EOF > keycloak.yaml
url: ${KEYCLOAK_URL:-http://keycloak:8080/auth/}
realm: ${KEYCLOAK_REALM:-sensor-db-controller}
client_id: ${KEYCLOAK_CLIENT_ID:-sensor-db-controller}
client_secret: ${KEYCLOAK_CLIENT_SECRET}
user: ${KEYCLOAK_USER:-sensor-db-controller-write}
password: ${KEYCLOAK_PASSWORD}
EOF

echo "Install cron job with schedule $CRON_SCHEDULE."
echo "$CRON_SCHEDULE root /bin/bash /home/sensor-dealer.sh > /proc/1/fd/1 2>&1" > /etc/cron.d/docker-cron-job
cron -f
