# -*- coding: utf-8 -*-
"""
sensor-dealer

@author: Jan Brinkmann
"""

import yaml
from yaml.loader import SafeLoader

from keycloak import KeycloakOpenID

import requests
from requests import Timeout


timeout = 60
endpoint = '/insert?timestamp=%s&room=%s&temperature=%s&humidity=%s'
token = None
response = None

# Sensor dealer configuration
with open('/home/sensor-dealer.yaml') as c:
    sensor = yaml.load(c, Loader=SafeLoader)


# Keycloak authentification
with open('/home/keycloak.yaml') as c:
    keycloak = yaml.load(c, Loader=SafeLoader)

try:
    keycloak_openid = KeycloakOpenID(server_url='http://%s:8080/auth/' % keycloak['url'],
                                     realm_name=keycloak['realm'],
                                     client_id=keycloak['client_id'],
                                     client_secret_key=keycloak['client_secret'],
                                     verify=False)
    token = keycloak_openid.token(keycloak['user'],
                                  keycloak['password'])
except:
    print('Keycloak authentication failed.')


# Request SensorBroker
if token is not None:
    try:
        response = requests.get(url=sensor['broker'],
                                timeout=timeout)
        if response.ok:
            data = response.json()

    except Timeout:
        print('Request to SensorBroker not succesfully due to a timeout.')
    except:
        print('Request to SensorBroker not succesfully.')


# Request SensorDbController
if response is not None:
    if data['temperature'] is not None and data['humidity'] is not None:
        try:
            print(sensor['db'] + endpoint % (data['timestamp'], sensor['room'], data['temperature'], data['humidity']))
            insert = requests.get(sensor['db'] + endpoint % (data['timestamp'], sensor['room'], data['temperature'], data['humidity']),
                                  headers={'Authorization': 'Bearer ' + token['access_token']},
                                  verify=True)
            print(response)

        except Timeout:
            print('Request to SensorDbController not succesfully due to a timeout.')
        except:
            print('Request to SensorDbController not succesfully.')

    else:
        print('SensorBroker sent Null data.')
