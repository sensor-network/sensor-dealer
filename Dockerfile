FROM ubuntu:20.04

MAINTAINER Jan Brinkmann

RUN apt-get update 
RUN apt-get install -y cron python3-pip

COPY requirements.txt /home/
RUN pip install -r /home/requirements.txt

COPY ./entry.sh /home/
COPY ./sensor-dealer.sh /home/
COPY ./sensor-dealer.py /home/
RUN chmod 755 /home/entry.sh
RUN chmod 755 /home/sensor-dealer.sh

WORKDIR /home
CMD [ "/home/entry.sh" ]
